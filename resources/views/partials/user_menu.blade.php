@if(Auth::check())
    <aside class="Panel">
        <figure class="Profile">
            <div class="Profile-avatar">
                @if(Auth::user()->picture)
                    <img src="{{ Storage::url(Auth::user()->picture) }}" alt="{{ Auth::user()->name }}" width="100">
                @endif
            </div>
            <figcaption class="Profile-name">
                <h2>{{ Auth::user()->name }}</h2>
            </figcaption>
        </figure>
        <nav class="ProfileMenu">
            <ul class="ProfileMenu-listItem">
                <li class="ProfileMenu-item">
                    <a href="{{ route('users.profile') }}" class="ProfileMenu-link">Mi perfil</a>
                </li>
                <li class="ProfileMenu-item">
                    <a href="{{ route('subscriptions.subscribe') }}" class="ProfileMenu-link">Suscripción</a>
                </li>
                @if(Auth::user()->role_id === 1)
                    <li class="ProfileMenu-item">
                        <a href="{{ route('admin.users.index') }}" class="ProfileMenu-link"><b>Usuarios</b></a>
                    </li>
                    <li class="ProfileMenu-item">
                        <a href="{{ route('admin.series.index') }}" class="ProfileMenu-link"><b>Administrar series</b></a>
                    </li>
                    <li class="ProfileMenu-item">
                        <a href="{{ route('admin.series.create') }}" class="ProfileMenu-link"><b>Añadir serie</b></a>
                    </li>
                    <li class="ProfileMenu-item">
                        <a href="{{ route('admin.prices.index') }}" class="ProfileMenu-link"><b>Precios</b></a>
                    </li>
                @endif
                <li class="ProfileMenu-item">
                    <a href="{{ route('explore') }}" class="ProfileMenu-link">Explorar</a>
                </li>
                <li class="ProfileMenu-item">
                    <a href="{{ route('users.historical') }}" class="ProfileMenu-link">Mi historial</a>
                </li>
                <li class="ProfileMenu-item">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                       class="ProfileMenu-link">
                        Cerrar sesión
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </nav>
    </aside>
@endif
